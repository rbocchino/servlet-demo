package talent.campus;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWorld extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private String message;

	@Override
	public void init() throws ServletException {

		this.message = "Hello World";
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println("Init servlet param: " + this.getInitParameter("param1"));

		System.out.println("Context param name: " + this.getServletContext().getInitParameter("name"));
		System.out.println("Context param email: " + this.getServletContext().getInitParameter("email"));

		System.out.println("Context path: " + request.getContextPath());
		
		System.out.println("Path info: " +request.getPathInfo());
		System.out.println("Remote Address: " +request.getRemoteAddr());
		System.out.println("Remote Address: " +request.getRemotePort());

		System.out.println("Local Address: " +request.getLocalAddr());
		System.out.println("Local Address: " +request.getLocalPort());

		System.out.println("Uri request: " +request.getRequestURI());
		
		System.out.println("Headers");
		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			String headerVal = request.getHeader(headerName);
			
			System.out.printf("%s=%s\n",headerName, headerVal);
			
		}
		
		System.out.println("Protocol " +  request.getProtocol());
		System.out.println("Query string " + request.getQueryString());
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.print("<h2>Hello " + this.message + "</h2>");
		out.close();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}