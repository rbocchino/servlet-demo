package talent.campus;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(value = "/InputForm", initParams = { @WebInitParam(name = "init_first_name", value = "empty"),
		@WebInitParam(name = "init_last_name", value = "empty") })
public class InputForm extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		String initName = this.getInitParameter("init_first_name");
		String initLastname = this.getInitParameter("init_last_name");

		testSession(req);
		
		
		Optional<Cookie> optional = Arrays.stream(req.getCookies())
				.filter(cookie -> cookie.getName().equals("full_name")).findFirst();

		String body = null;
		if (optional.isPresent()) {
			Cookie cookie = optional.get();
			String value = cookie.getValue();
			String[] nameLastname = value.split("_", 2);

			body = "<html><head><title>Hay Cookies</title></head>" + "<body bgcolor = \"#f0f0f0\">\n"
					+ "<h1 align = \"center\">Hay Cookies</h1><ul>" + "  <li><b>First Name</b>: " + nameLastname[0]
					+ "<li><b>Last Name</b>: " + nameLastname[1] + "</ul></body></html>";

		} else {
			body = "<html>\r\n" + "   <body>\r\n" + "      <form action = \"HelloForm\" method = \"POST\">\r\n"
					+ "         First Name: <input type = \"text\" name = \"first_name\" value=\"%s\"> \r\n"
					+ "         <br />\r\n"
					+ "         Last Name: <input type = \"text\" name = \"last_name\" value=\"%s\"/>\r\n"
					+ "         <input type = \"submit\" value = \"Submit\" />\r\n" + "      </form>\r\n"
					+ "   </body>\r\n" + "</html>";
		}
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.print(String.format(body, initName, initLastname));
		out.close();
	}

	private void testSession(HttpServletRequest req) {
		HttpSession session = req.getSession(false);
		if (session == null) {
			System.out.println("Sin session");
			session = req.getSession(true);
		}

		Date lastAccessTime = new Date(session.getLastAccessedTime());
		System.out.println("Last accessed time: " + lastAccessTime.toString());

		Integer counter = null;
		if (session.isNew()) {
			counter = 1;
			session.setAttribute("counter", counter);
		} else {
			counter = (Integer) session.getAttribute("counter");
			counter++;
			session.setAttribute("counter", counter);
		}

		System.out.println("ImputForm Access Counter: " + counter);
	}

}
